package GUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import model.BankAccount;

/**
   A frame that shows the growth of an investment with variable interest.
*/
public class InvestmentFrame extends JFrame
{    
  

   private static final double DEFAULT_RATE = 5;
   private static final double INITIAL_BALANCE = 1000;   

   private JLabel rateLabel;
   private JTextField rateField;
   private JButton button;
   private JLabel resultLabel;
   private JPanel panel;
   private BankAccount account;
  
   
   public InvestmentFrame()
   {  
	
	  createLaber("balance: " + INITIAL_BALANCE);
      createTextField();
      createButton();
      createPanel();
      
     
   }

   public void createTextField()
   {
      rateLabel = new JLabel("Interest Rate: ");

      final int FIELD_WIDTH = 10;
      rateField = new JTextField(FIELD_WIDTH);
      rateField.setText("" + DEFAULT_RATE);
   }
   
   public void createButton()
   {
      button = new JButton("Add Interest");
      
      
   }

   public void createPanel()
   {
      panel = new JPanel();
      panel.add(rateLabel);
      panel.add(rateField);
      panel.add(button);
      panel.add(resultLabel);      
      add(panel);
   } 
   public void createResultlabel(String text){
	   resultLabel = new JLabel(text);
	   
   }
   
   public void setListener(ActionListener list) {
		button.addActionListener(list);
	}
   public String getRateField(){
	   return  rateField.getText ();
   }

	public void setResult(String text) {
		resultLabel.setText(text);
	}
	public void createLaber(String text){
		resultLabel = new JLabel(text);
	   }
}
